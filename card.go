package cards

import (
	"fmt"
	"gitlab.com/atrico/cards/rank"
	"gitlab.com/atrico/cards/suit"
	"sort"
)

type Card struct {
	Rank rank.Rank
	Suit suit.Suit
}

func (c Card) String() string {
	return fmt.Sprintf("%v%v", c.Rank.Char(), c.Suit.Char())
}

func Parse(txt string) Card {
	var rk, st string
	if len(txt) == 2 {
		rk = string(txt[0])
		st = string(txt[1])
	} else if len(txt) == 3 {
		rk = txt[0:2]
		st = string(txt[2])
	}
	return Card{rank.Parse(rk), suit.Parse(st)}
}

// Comparable
func (c Card) Compare(rhs Card) int {
	rankComp := c.Rank.Compare(rhs.Rank)
	switch {
	case rankComp == 0:
		return c.Suit.Compare(rhs.Suit)
	default:
		return rankComp
	}
}

// Sort cards into a canonical order
// (highest first)
func Sort(hand []Card) {
	sort.Slice(hand, func(i, j int) bool {
		return hand[i].Compare(hand[j]) > 0
	})
}
