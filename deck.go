package cards

import (
	"errors"
	"gitlab.com/atrico/cards/rank"
	"gitlab.com/atrico/cards/suit"
	"gitlab.com/atrico/core/patterns"
	"golang.org/x/net/context"
)

type Deck interface {
	patterns.Iteratable[Card]
	// Remaining cards
	Count() int
	// Deal some cards
	DealCards(count int) []Card
	// Deal some hands
	DealHands(players, count int) [][]Card
}

func CreateDeck() Deck {
	d := deck{make([]Card, 0, 52)}
	for _, s := range suit.AllSuits {
		for _, r := range rank.AllRanks {
			d.cards = append(d.cards, Card{r, s})
		}
	}
	return &d
}

var NotEnoughCards = errors.New("not enough cards")

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type deck struct {
	cards []Card
}

func (d *deck) Iterator(ctx context.Context) <-chan Card {
	ch := make(chan Card)
	go func() {
		defer close(ch)
		for _, c := range d.cards {
			select {
			case <-ctx.Done():
				return
			case ch <- c:
			}
		}
	}()
	return ch
}

func (d *deck) Count() int {
	return len(d.cards)
}

func (d *deck) DealCards(count int) (cards []Card) {
	if count > len(d.cards) {
		panic(NotEnoughCards)
	}
	cards = make([]Card, count)
	copy(cards, d.cards)
	d.cards = d.cards[count:]
	return
}

func (d *deck) DealHands(players, count int) (hands [][]Card) {
	if players*count > len(d.cards) {
		panic(NotEnoughCards)
	}
	hands = make([][]Card, players)
	for i := range hands {
		hands[i] = make([]Card, count)
	}
	card := 0
	for i := 0; i < count; i++ {
		for p := 0; p < players; p++ {
			hands[p][i] = d.cards[card]
			card++
		}
	}
	d.cards = d.cards[card:]
	return
}
