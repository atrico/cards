package poker

import (
	"gitlab.com/atrico/cards"
)

type ScoreType byte

const (
	High ScoreType = iota
	Pair
	TwoPairs
	ThreeOfAKind
	Straight
	Flush
	FullHouse
	FourOfAKind
	StraightFlush
	RoyalFlush
)

type Score []byte

func ScoreHand(hand []cards.Card) (score Score) {
	cards.Sort(hand)
	for _, sf := range scoreTypes {
		score = sf(hand)
		if score != nil {
			return
		}
	}
	return scoreHigh(hand)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
var scoreTypes = []func(hand []cards.Card) Score{
	scoreRoyalFlush,
	scoreStraightFlush,
	scoreFourOfAKind,
	scoreFullHouse,
	scoreFlush,
	scoreStraight,
	scoreThreeOfAKind,
	scoreTwoPairs,
	scorePair,
}

func scorePair(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreThreeOfAKind(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreTwoPairs(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreStraight(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreFlush(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreFullHouse(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreFourOfAKind(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreStraightFlush(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreRoyalFlush(hand []cards.Card) Score {
	// TODO
	return nil
}

func scoreHigh(hand []cards.Card) Score {
	// All the cards in order
	return makeScore(High, hand)
}

func (s Score) Compare(rhs Score) int {
	for i, v := range s {
		if v < rhs[i] {
			return -1
		} else if v > rhs[i] {
			return 1
		}
	}
	return 0
}

func makeScore(typ ScoreType, cs []cards.Card) Score {
	score := make([]byte, len(cs)+1)
	score[0] = byte(typ)
	for i, c := range cs {
		score[i+1] = byte(c.Rank)
	}
	return score
}
