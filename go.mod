module gitlab.com/atrico/cards

go 1.20

require (
	gitlab.com/atrico/core v1.28.4
	gitlab.com/atrico/testing/v2 v2.4.1
	golang.org/x/net v0.10.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/rs/zerolog v1.29.1 // indirect
	golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
