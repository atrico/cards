package suit

import (
	"errors"
	"strings"
)

type Suit byte
type Colour bool

const (
	Hearts Suit = iota
	Diamonds
	Spades
	Clubs

	Red   Colour = false
	Black Colour = true
)

var AllSuits = []Suit{Hearts, Diamonds, Spades, Clubs}

func (s Suit) String() string {
	switch s {
	case Hearts:
		return "Hearts"
	case Diamonds:
		return "Diamonds"
	case Spades:
		return "Spades"
	case Clubs:
		return "Clubs"
	default:
		panic(Invalid)
	}
}

func (s Suit) Char() string {
	switch s {
	case Hearts:
		return "♥"
	case Diamonds:
		return "♦"
	case Spades:
		return "♠"
	case Clubs:
		return "♣"
	default:
		panic(Invalid)
	}
}

func (s Suit) Colour() Colour {
	switch s {
	case Hearts, Diamonds:
		return Red
	case Spades, Clubs:
		return Black
	default:
		panic(Invalid)
	}
}

func Parse(txt string) Suit {
	switch strings.ToUpper(txt) {
	case "H", "♥":
		return Hearts
	case "D", "♦":
		return Diamonds
	case "S", "♠":
		return Spades
	case "C", "♣":
		return Clubs
	default:
		panic(Invalid)
	}
}

// Comparable
func (s Suit) Compare(rhs Suit) int {
	return int(s) - int(rhs)
}

var Invalid = errors.New("invalid suit")
