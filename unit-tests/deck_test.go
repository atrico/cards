package unit_tests

import (
	"context"
	"gitlab.com/atrico/cards"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/core/collection/set"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_Deck_Create(t *testing.T) {
	// Act
	deck := cards.CreateDeck()

	// Assert
	assert.That(t, deck.Count(), is.EqualTo(52), "size")
}
func Test_Deck_All(t *testing.T) {
	// Arrange
	deck := cards.CreateDeck()

	// Act
	all := core.ReadChannelAsSlice(deck.Iterator(context.Background()))

	// Assert
	assert.That(t, len(all), is.EqualTo(52), "length")
	assert.That(t, allUnique(all), is.True, "unique")
}

func Test_Deck_DealCards(t *testing.T) {
	// Arrange
	const count = 10
	deck := cards.CreateDeck()

	// Act
	hand := deck.DealCards(count)
	t.Log(hand)

	// Assert
	assert.That(t, len(hand), is.EqualTo(count), "size")
	assert.That(t, allUnique(hand), is.True, "unique")
	assert.That(t, deck.Count(), is.EqualTo(52-count), "remaining")
}

func Test_Deck_DealCardsNotEnough(t *testing.T) {
	// Arrange
	const count = 53
	deck := cards.CreateDeck()

	// Act
	err := PanicCatcher(func() { deck.DealCards(count) })

	// Assert
	assert.That(t, err, is.NotNil, "error")
	assert.That(t, err.(error), is.EqualTo(cards.NotEnoughCards), "error type")
}

func Test_Deck_DealHands(t *testing.T) {
	// Arrange
	const hands = 3
	const count = 5
	deck := cards.CreateDeck()

	// Act
	result := deck.DealHands(hands, count)
	t.Log(result)

	// Assert
	assert.That(t, len(result), is.EqualTo(hands), "no of hands")
	for i, hnd := range result {
		assert.That(t, len(hnd), is.EqualTo(count), "size: %d", i)
		assert.That(t, allUnique(hnd), is.True, "unique: %d", i)
	}
	assert.That(t, deck.Count(), is.EqualTo(52-(count*hands)), "remaining")
}

func Test_Deck_DealHandsNotEnough(t *testing.T) {
	// Arrange
	const hands = 5
	const count = 13
	deck := cards.CreateDeck()

	// Act
	err := PanicCatcher(func() { deck.DealHands(hands, count) })

	// Assert
	assert.That(t, err, is.NotNil, "error")
	assert.That(t, err.(error), is.EqualTo(cards.NotEnoughCards), "error type")
}

func allUnique(hand []cards.Card) bool {
	st := set.MakeSet(hand...)
	return len(hand) == st.Len()
}
