package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/cards"
	"gitlab.com/atrico/cards/games/poker"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_PokerScore(t *testing.T) {
	testImpl := func(handL, handR []cards.Card, matcher assert.Matcher[int]) {
		// Act
		scoreL := poker.ScoreHand(handL)
		scoreR := poker.ScoreHand(handR)

		// Assert
		assert.That(t, scoreL.Compare(scoreR), matcher, "compare")
	}
	for nameL, testCaseL := range pokerScoreLessTestCases {
		t.Run(nameL, func(t *testing.T) {
			for nameR, rhs := range testCaseL.less {
				t.Run(fmt.Sprintf("%s (less)", nameR), func(t *testing.T) {
					testImpl(testCaseL.lhs, rhs, is.GreaterThan(0))
				})
			}
			for nameR, rhs := range testCaseL.equal {
				t.Run(fmt.Sprintf("%s (equal)", nameR), func(t *testing.T) {
					testImpl(testCaseL.lhs, rhs, is.EqualTo(0))
				})
			}
			for nameR, rhs := range testCaseL.more {
				t.Run(fmt.Sprintf("%s (more)", nameR), func(t *testing.T) {
					testImpl(testCaseL.lhs, rhs, is.LessThan(0))
				})
			}
		})
	}
}

var pokerScoreLessTestCases = map[string]pokerScoreLessTestCase{
	"high": pokerScoreLessTestCase{
		lhs: makeHand("AH", "3D", "4S", "5C", "6H"),
		less: makeHands(
			makeHandN("high", "AH", "2D", "4S", "5C", "6H"),
		),
		equal: makeHands(
			makeHandN("high", "AD", "3H", "4C", "5S", "6H"),
		),
		more: makeHands(
			makeHandN("high", "AH", "3D", "4S", "5C", "7H"),
			makeHandN("pair", "AH", "AD", "4S", "5C", "6H"),
			makeHandN("two-pair", "AH", "AD", "4S", "4C", "6H"),
			makeHandN("3", "AH", "AD", "AS", "5C", "6H"),
			makeHandN("straight", "AH", "2C", "3H", "4H", "5H"),
			makeHandN("flush", "AH", "7H", "3H", "4H", "5H"),
			makeHandN("full-house", "AH", "AD", "AS", "5C", "5H"),
			makeHandN("4", "AH", "AD", "AS", "AC", "6H"),
			makeHandN("straight-flush", "AH", "2H", "3H", "4H", "5H"),
			makeHandN("royal-flush", "AH", "KH", "QH", "JH", "10H"),
		),
	},
	"pairK": pokerScoreLessTestCase{
		lhs: makeHand("KH", "KD", "4S", "5C", "6H"),
		less: makeHands(
			makeHandN("pairQ", "QS", "QC", "4S", "5C", "6H"),
			makeHandN("pairK", "KS", "KC", "3S", "5C", "6H"),
		),
		equal: makeHands(
			makeHandN("pairK", "KS", "KC", "4S", "5C", "6H"),
		),
		more: makeHands(
			makeHandN("pairA", "AS", "AC", "4S", "5C", "6H"),
			makeHandN("pairK", "KS", "KC", "7S", "5C", "6H"),
			makeHandN("two-pair", "AH", "AD", "4S", "4C", "6H"),
			makeHandN("3", "AH", "AD", "AS", "5C", "6H"),
			makeHandN("straight", "AH", "2C", "3H", "4H", "5H"),
			makeHandN("flush", "AH", "7H", "3H", "4H", "5H"),
			makeHandN("full-house", "AH", "AD", "AS", "5C", "5H"),
			makeHandN("4", "AH", "AD", "AS", "AC", "6H"),
			makeHandN("straight-flush", "AH", "2H", "3H", "4H", "5H"),
			makeHandN("royal-flush", "AH", "KH", "QH", "JH", "10H"),
		),
	},
}

type pokerScoreLessTestCase struct {
	lhs               []cards.Card
	less, equal, more map[string][]cards.Card
}

type namedHand struct {
	name string
	hand []cards.Card
}

func makeHand(strs ...string) (hand []cards.Card) {
	for _, c := range strs {
		hand = append(hand, cards.Parse(c))
	}
	return
}
func makeHandN(name string, cards ...string) (hand namedHand) {
	hand.name = name
	hand.hand = makeHand(cards...)
	return
}

func makeHands(hands ...namedHand) (testCase map[string][]cards.Card) {
	testCase = make(map[string][]cards.Card, len(hands))
	for _, rhs := range hands {
		testCase[rhs.name] = rhs.hand
	}
	return
}
